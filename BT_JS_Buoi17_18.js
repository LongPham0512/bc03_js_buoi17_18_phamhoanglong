var numberArr = [];
function nhapDaySo(numberValue) {
  var numberValue = document.getElementById("txt_number").value * 1;

  numberArr.push(numberValue);

  console.log({ numberArr });
  document.getElementById("day_number").innerHTML = `<p>${numberArr}</p>`;
}

// BÀI 1: Tổng các số dương trong mảng.
function TinhTongSoDuong() {
  var TongSoDuong = 0;
  var soDuongArr = [];
  var soAmArr = [];

  numberArr.forEach(function (item) {
    if (item < 0) {
      soAmArr.push(item);
    } else {
      soDuongArr.push(item);
    }
  });

  soDuongArr.forEach(function (item) {
    TongSoDuong += item * 1;
  });

  console.log({ soAmArr, soDuongArr, TongSoDuong });
  document.getElementById("ket_qua1").innerHTML = `<span>${TongSoDuong}</span>`;
}

// Bài 2:Đếm có bao nhiêu số dương trong mảng.
function demSoDuong() {
  var count = 0;

  numberArr.forEach(function (item) {
    if (item > 0) {
      count++;
    }
    console.log({ count });
  });
  document.getElementById("ket_qua2").innerHTML = `<span> ${count} </span>`;
}

// Bài 3: Tìm số nhỏ nhất trong mảng.
function timSoNhoNhat() {
  var soNhoNhat = null;
  numberArr.sort(function (a, b) {
    return a - b;
  });
  soNhoNhat = numberArr[0];
  console.log(numberArr, soNhoNhat);

  document.getElementById("ket_qua3").innerHTML = `<span> ${soNhoNhat} </span>`;
}

//Bài 4: Tìm số dương nhỏ nhất trong mảng.
function timSoDuongNhoNhat() {
  var soDuongNhoNhat = null;
  var soDuongArr = [];
  numberArr.forEach(function (item) {
    if (item > 0) {
      soDuongArr.push(item);
    }
  });

  soDuongArr.sort(function (a, b) {
    return a - b;
  });
  soDuongNhoNhat = soDuongArr[0];

  document.getElementById(
    "ket_qua4"
  ).innerHTML = `<span> ${soDuongNhoNhat} </span>`;
}

//Bài 5: Tìm số chẵn cuối cùng trong mảng. Nếu mảng không có giá trị chẵn thì trả về -1.
function timSoChanCuoiCung() {
  var soChanCuoiCung = 0;

  // var soChanArr = [];

  // numberArr.forEach(function (item) {
  //   if (item % 2 == 0) {
  //     soChanArr.push(item);
  //     console.log({ item });
  //   }
  // });
  // console.log(soChanArr);

  // soChanCuoiCung = soChanArr[soChanArr.length - 1];
  // console.log({ soChanCuoiCung });

  // document.getElementById(
  //   "ket_qua5"
  // ).innerHTML = `<span>${soChanCuoiCung}</span>`;

  for (var i = numberArr.length - 1; i >= 0; i--) {
    if (numberArr[i] % 2 == 0) {
      soChanCuoiCung = numberArr[i];
      break;
    }
  }
  console.log({ soChanCuoiCung });
  document.getElementById(
    "ket_qua5"
  ).innerHTML = `<span>${soChanCuoiCung}</span>`;
}

// Bài 6:Đổi chỗ 2 giá trị trong mảng theo vị trí.
function doiViTri() {
  var viTri1 = document.getElementById("txt_vitri1").value * 1;
  var viTri2 = document.getElementById("txt_vitri2").value * 1;

  if (
    viTri1 < 0 ||
    viTri2 < 0 ||
    viTri1 > numberArr.length ||
    viTri2 > numberArr.length
  ) {
    alert("Nhập lại vị trí");
  } else {
    const tmp = numberArr[viTri1 - 1];
    numberArr[viTri1 - 1] = numberArr[viTri2 - 1];
    numberArr[viTri2 - 1] = tmp;
    console.log({ numberArr });
  }

  document.getElementById("ket_qua6").innerHTML = `${numberArr}`;
}

// Bài 7: Sắp xếp mảng theo thứ tự tăng dần.
function sapXepTangDan() {
  var TangDanArr = "";
  TangDanArr = numberArr.sort(function (a, b) {
    return a - b;
  });

  document.getElementById("ket_qua7").innerHTML = `<span>${TangDanArr}</span>`;
}

//Bài 8: Tìm số nguyên tố đầu tiên trong mảng. Nếu mảng không có số nguyên tố thì trả về – 1.
function timSoNguyenToDauTien() {
  var soNguyenToDauTien = null;
  var soNguyenToArr = [];

  numberArr.forEach(function timSoNguyenTo(num) {
    if (num <= 1) {
      return false;
    }
    for (var i = 2; i <= num / 2; i++) {
      if (num % 2 == 0) {
        return false;
      }
      return soNguyenToArr.push(num);
    }
  });
  soNguyenToDauTien = soNguyenToArr[0];
  console.log({ soNguyenToArr, soNguyenToDauTien });
  document.getElementById(
    "ket_qua8"
  ).innerHTML = `<span>${soNguyenToDauTien}</span>`;
}

//Bài 9:Nhập thêm 1 mảng số thực, tìm xem trong mảng có bao nhiêu số nguyên?
var numberArr2 = [];
function nhapDaySoThucMoi(numberValue2) {
  var numberValue2 = document.getElementById("txt_nhapSo").value * 1;
  numberArr2.push(numberValue2);

  console.log({ numberArr2 });

  var NewArr = numberArr.concat(numberArr2);

  var SoNguyenMoiArr = [];
  NewArr.forEach(function (item) {
    if (Number.isInteger(item) == true) {
      return SoNguyenMoiArr.push(item);
    }
  });
  console.log({ SoNguyenMoiArr });

  //xuất dãy mới
  document.getElementById(
    "day_so_moi"
  ).innerHTML = /**html */ `<div><p>Dãy số thực: ${numberArr2}</p>
  <p>Dãy Số mới: ${NewArr}</p>
  <p>Số Lượng số nguyên trong mảng mới: ${SoNguyenMoiArr.length}</p></div>`;
}

//Bài 10: So sánh số lượng số dương và số lượng số âm xem số nào nhiều hơn.
function soSanhSoAmDuong() {
  var daySoDuongArr = [];
  var daySoAmArr = [];

  numberArr.forEach(function (item) {
    if (item > 0) {
      daySoDuongArr.push(item);
    } else {
      daySoAmArr.push(item);
    }
  });
  console.log({ daySoDuongArr, daySoAmArr });

  var soSanh = null;
  if (daySoDuongArr.length == daySoAmArr.length) {
    soSanh = "Số Âm = Số Dương";
  } else if (daySoDuongArr.length > daySoAmArr.length) {
    soSanh = "Số Âm < Số Dương";
  } else {
    soSanh = "Số Âm > Số Dương";
  }
  console.log({ soSanh });

  document.getElementById("ket_qua10").innerHTML = `${soSanh}`;
}
